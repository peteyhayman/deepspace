
function createDrawer(params) {
    const newDrawer = {
        functions: [createFunction()],
        size: 2,
        x: 0,
        y: 0,
        color: color(0, 255, 255),
        width: width,
        height: height,

        minWH: function () { return this.width < this.height ? this.width : this.height },
        callback: drawLinear,
        //creates new instance of this function every time
        draw: function () {
            //const funcArrays = this.functions.reduce((rd, f) => {
            //   return rd.concat([f.toArray()])
            //}, [])
            const funcArrays = this.functions.reduce((fArr, f) => {
                return f.appendArray(fArr)
            }, [])
            stroke(this.color)
            strokeWeight(this.size)
            this.callback(funcArrays)
        }

    }
    return Object.assign(newDrawer, params)
}


function drawLinear(resultsData) {
    push()
    translate(this.x, this.y + this.height / 2)
    resultsData2Points(resultsData, this.width).
        forEach(p =>
            point(p.x, p.y * this.height / 3))
    pop()
}

function drawRadial(resultsData) {
    push()
    translate(this.x + this.width / 2, this.y + this.height / 2)
    const radius = this.minWH() / 6
    //a point for each unit in the circumference
    const numPoints = TWO_PI * radius

    resultsData2Points(resultsData, numPoints).
        forEach(p => {
            const theta = p.x / numPoints * TWO_PI
            const x = cos(theta) * (radius + radius * p.y * 0.5)
            const y = sin(theta) * (radius + radius * p.y * 0.5)
            point(x, y)
        })
    pop()
}


function drawParametric(resultsData) {
    push()
    translate(this.x + this.width / 2, this.y + this.height / 2)
    const radius = this.minWH() / 6
    const numPoints = TWO_PI * radius

    const xPoints = results2Points(resultsData[0], numPoints)
    const yPoints = results2Points(resultsData[1], numPoints)
    for (let i = 0; i < numPoints; i++) {
        point(xPoints[i].y * radius, yPoints[i].y * radius)
    }
    pop()
}

function drawParametricPolar(parametricSet) {
    push()
    translate(this.x + this.width / 2, this.y + this.height / 2)
    const radius = this.minWH() / 6
    const numPoints = parametricSet[0].length * 7


    const thetaPoints = results2Points(parametricSet[0], numPoints)
    const radiusPoints = results2Points(parametricSet[1], numPoints)

    thetaPoints.forEach((theta, i) => {
        point(cos(theta.y) * radiusPoints[i].y * radius, sin(theta.y) * radiusPoints[i].y * radius)
    }
    )
    pop()
}

function drawLinearSoft(resultsData) {
    const pixelPeak = this.height / 6
    const lines = points2Lines(
        resultsData2Points(resultsData, this.width),
        this.height, pixelPeak)

    for (let x = 0; x < this.width; x++) {
        for (let y = 0; y < this.height; y++) {
            const col = lines[x][y] * 255
            stroke(0, 255 - col, 255 - col)
            point(x, y)
        }
    }
}


function points2Lines(points, lineLength, pixelPeak) {
    const mid = lineLength / 2

    return points.map((p, i) => {
        const normalPointY = p.y * pixelPeak + mid
        const lineArray = []

        for (let y = 0; y < lineLength; y++) {
            const distance = abs(normalPointY - y)
            const normalDistance = constrain(distance, 0, pixelPeak) / pixelPeak

            lineArray.push(normalDistance)
        }
        return lineArray
    })
}

function resultsData2Points(resultsData, numPoints) {
    return resultsData.reduce((p, r) =>
        p.concat(results2Points(r, numPoints)), [])
}

function results2Points(results, numPoints) {
    const deltaIndex = results.length / numPoints
    const points = []
    for (let x = 0; x < numPoints; x++) {
        const iFloat = x * deltaIndex
        const i0 = floor(iFloat)
        const i1 = (i0 + 1) % results.length //this wraps around the values
        const t = iFloat % 1
        const y = (results[i1] - results[i0]) * t + results[i0]
        points.push({ x: x, y: y })
    }
    return points
}
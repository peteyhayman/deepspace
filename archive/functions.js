

function createLissajoursPattern(params) {
    const newPattern = {
        functions: [
            createFunction({
                m: 3,
                resolution: 32,
                callback: function (theta) {
                    return sin(theta * this.m)
                }
            }),
            createFunction({
                n: 2,
                callback: function (theta) {
                    return sin(theta * this.n)
                }
            })
        ]
    }
    return Object.assign(
        createParametricFunction(),
        newPattern,
        params)
}

function createRose(params) {
    const newRose = {
        functions: [
            createFunction({
                n: 2,
                d: 3,
                rotations: 3,
                callback: function (theta) {
                    return cos((this.n / this.d) * theta) * cos(theta)
                }
            }),
            createFunction({
                n: 2,
                d: 3,
                rotations: 3,
                callback: function (theta) {
                    return cos((this.n / this.d) * theta) * sin(theta)
                }
            })
        ]
    }
    return Object.assign(
        createParametricFunction(),
        newRose,
        params
    )

}

function createParametricFunction(params) {
    const newParametricFunction = {
        functions: [
            createFunction({ callback: cos }),
            createFunction()
        ],
        appendArray: function (appArr) {
            return this.functions.reduce((arr, f) =>
                f.appendArray(arr), appArr)
        }
    }
    return Object.assign(
        newParametricFunction,
        params)
}


function createAdditiveFunction(params) {
    const newAdditiveFunction = {
        functions: [],
        toArray: function () {
            return this.functions.reduce((arr, f) => {
                //half the amplitudes to offset the addition
                const newArr = f.toArray().reduce((facc, r, i) => {
                    if (arr[i] === undefined)
                        arr[i] = 0
                    return facc.concat(arr[i] + r / 2)
                }, [])
                return newArr
            }, [])
        }
    }
    return Object.assign(
        createFunction(),
        newAdditiveFunction,
        params)
}

function createFunction(params) {
    const newFunction = {
        phase: 0,
        frequency: 1,
        amplitude: 1,
        callback: sin,
        solve: function (x) {
            return this.callback(x * this.frequency + this.phase) * this.amplitude
        },
        rotations: 3,
        resolution: 64,
        toArray: function () {
            const vals = []
            const deltaAngle = (this.rotations * TWO_PI) / this.resolution
            for (let i = 0; i < this.resolution; i++) {
                const theta = i * deltaAngle
                //perhaps we should pass in i * delta angle to val, so it remembers its angle
                const val = this.solve(theta)
                vals.push(val)
            }
            return vals
        },
        appendArray(arr) {
            return arr.concat([this.toArray()])
        }
    }
    return Object.assign(
        newFunction,
        params)
}

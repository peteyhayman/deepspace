

function createDeck() {
  const allPatterns = []
  const allVisualizers = []
  const allGenerators = []

  const newVisualizer = createVisualizer({ name: 'Visualizer ' + allVisualizers.length })
  const newPattern = createPattern({ name: 'Pattern ' + allPatterns.length })

  const newGenerator = createGenerator({ name: 'Generator ' + allGenerators.length })
  const newGenerator2 = createGenerator({ name: 'Generator ' + allGenerators.length })

  allGenerators.push(newGenerator)
  allGenerators.push(newGenerator2)

  newPattern.inputs.push(newGenerator)
  newPattern.inputs.push(newGenerator2)
  allPatterns.push(newPattern)

  newVisualizer.inputs.push(newPattern)
  allVisualizers.push(newVisualizer)


  createTable(allGenerators, allPatterns, allVisualizers)

}

function createTable(arrLeft, arrMid, arrRight) {

  const myTable = document.createElement("TABLE")
  document.body.appendChild(myTable)
  appendHeaders(arrLeft, arrMid, arrRight, myTable)
  appendBody(arrLeft, arrMid, arrRight, myTable)

  function appendHeaders(arr1, arr2, arr3, table) {
    table.appendChild(document.createElement("TR"))
    appendHeader(arr1, table)
    const hmid = document.createElement("TH")
    hmid.innerHTML = arr2.constructor.name
    table.appendChild(hmid)
    appendHeader(arr3, table)
  }

  function appendBody(arr1, arr2, arr3, table) {
    arr2.forEach((p, i) => {
      table.appendChild(document.createElement('TR'))
      appendRow(arr1, p, myTable, 'to')
      const hmid = document.createElement("td")
      hmid.innerHTML = p.name
      table.appendChild(hmid)
      appendRow(arr3, p, myTable, 'from')
    })
  }

  function appendHeader(array, table) {
    array.forEach(e => {
      const th = document.createElement("TH")
      th.innerHTML = e.name
      table.appendChild(th)
    })
  }

  function appendRow(array, rowObj, table, direction) {
    return array.map(colObj => {
      const cell = document.createElement("td")
      const checkbox = document.createElement("input")
      checkbox.type = "checkbox"
      checkbox.addEventListener("input",
        function () {
          const receiver = direction === 'to' ? rowObj : colObj
          const giver = direction === 'to' ? colObj : rowObj
          const index = receiver.inputs.indexOf(giver)
          //console.log(receiver, giver)

          if (checkbox.checked && index === -1)
            receiver.inputs.push(giver)
          if (!checkbox.checked && index !== -1)
            receiver.inputs.splice(index, 1)
          //console.log(receiver.inputs)
        }
      )
      cell.appendChild(checkbox)
      table.appendChild(cell)
    })
  }
}
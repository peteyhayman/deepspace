
function createVisualizer(properties) {
  const visualizer = {
    animator: createAnimator({ value: true }),
    inputs: [],
    size: 4,
    resolution: { value: 128, min: 1, step: 1 },
    scale: { value: 0.9 },
    color: { r: 0, g: 255, b: 255 },
    coordinateSystem: {
      value: sets2Polar,
      elementType: 'SELECT',
      optionsAttributes: allCoordinateSystems
    }
  }

  visualizer.render = function () {
    const sets = visualizer.inputs.reduce((acc, p) =>
      p.appendSets(acc)
      , [])

    const interpolatedSets = sets.map(s =>
      set2InterpolatedSet(s, visualizer.resolution.value))

    const positionSets = visualizer.coordinateSystem.value(interpolatedSets, visualizer.scale.value)
    positionSets2Canvas(positionSets, visualizer.canvas)
  }

  visualizer.animator.updateCallbacks.push(visualizer.render)
  Object.assign(visualizer, properties)
  visualizer.domAdvocate = createDomAdvocate(visualizer)
  visualizer.canvas = visualizer.domAdvocate.children[1]
  return visualizer
}

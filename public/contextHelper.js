function setBackground(cnv, fillStyle) {
  const ctx = cnv.getContext('2d')
  ctx.fillStyle = fillStyle
  ctx.fillRect(0, 0, cnv.width, cnv.height)
}

//points are anticipated to be normalized cartesian objects with x & y components
function positionSets2Canvas(positionSets, cnv) {
  //temp
  setBackground(cnv, 'green')
  const ctx = cnv.getContext('2d');
  ctx.imageSmoothingEnabled = true;
  // const pixels = positionSets.reduce((acc, p) =>
  // acc.concat(positions2Pixels(cnv, p))
  // , [])
  const pixelSets = positionSets.map(s => positions2Pixels(cnv, s))

  pixelSets.forEach((pixels, i) => {
    ctx.fillStyle = 'blue'
    ctx.beginPath()
    ctx.moveTo(pixels[0].x, pixels[0].y)
    pixels.slice(1, pixels.length).forEach(p => {
      //ctx.fillRect(p.x - 1, p.y - 1, 2, 2)
      ctx.lineTo(p.x, p.y)
    })
    if (i > 0) {
      const finalPoint = pixelSets[i - 1][0]
      //ctx.lineTo(finalPoint.x, finalPoint.y)
    }
    ctx.stroke()
  })
}



//this will invert the y axis
function positions2Pixels(canvas, positions) {
  return positions.map(p => ({
    x: p.x * canvas.width,
    y: (1 - p.y) * canvas.height
  })
  )
}

function brokenimagedata() {
  //this should include blending
  const i = (p.x + p.y * cnv.width) * 4
  //console.log(p, i)

  const imgData = ctx.getImageData(0, 0, cnv.width, cnv.height)
  imgData.data[i] = 127;
  imgData.data[i + 1] = 0;
  imgData.data[i + 2] = 127;
  imgData.data[i + 3] = 255;

  ctx.putImageData(imgData, 0, 0)
}
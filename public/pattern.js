

function createPattern(properties) {
  const pattern = {
    inputs: [],
    cycles: { value: 1, min: 1, max: 12, step: 1 },
    resolution: { value: 12, min: 1, max: 256, step: 1 },
  }

  pattern.appendSets = function (sets) {
    const newSets = pattern.inputs.map(generator => {
      const vals = []
      const deltaTheta = (pattern.cycles.value * Math.PI * 2) / pattern.resolution.value
      for (let i = 0; i < pattern.resolution.value; i++) {
        vals.push(generator.generate(i * deltaTheta))
      }
      return vals
    })
    return sets.concat(newSets)
  }


  Object.assign(pattern, properties)
  pattern.domAdvocate = createDomAdvocate(pattern)
  return pattern
}






//operation names must be identical to their value string
const allOperations = {
  sin: {
    text: "Sine",
    value: "sin",
    callback: Math.sin
  },
  cos: {
    text: "Cosine",
    value: "cos",
    callback: Math.cos
  },
  revsin: {
    text: "Reverse Sine",
    value: "revsin",
    callback: (x) => Math.sin(x)
  }
}

function createGenerator(properties) {
  const generator = {
    animate: createAnimator(),
    speed: { value: 1, min: -2, max: 2 },
    phase: { value: 0, max: Math.PI * 2 },
    frequency: { value: 1, max: 200 },
    amplitude: { value: 1 },
    operation: {
      value: Math.sin,
      elementType: 'SELECT',
      optionsAttributes: allOperations
    }
  }

  generator.generate = function (x) {
    const timeScalar = generator.animate.value ? (Date.now() * 0.01) * generator.speed.value : 0
    const theta = generator.phase.value + x + timeScalar
    return generator.operation.value(theta * generator.frequency.value) * generator.amplitude.value
  }

  Object.assign(generator, properties)
  generator.domAdvocate = createDomAdvocate(generator)

  return generator
}
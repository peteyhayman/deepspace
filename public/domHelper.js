
function createDomAdvocate(obj) {
  const bodyElt = createElement(
    'div', { className: 'dom advocate' })

  const paramElt = createParameterAdvocate(obj)
  bodyElt.appendChild(paramElt)

  //change this to is a visualizer
  if (obj.hasOwnProperty('render')) {
    const canvasElt = createElement('canvas')
    bodyElt.appendChild(canvasElt)
    bodyElt.className += " visualizer"
    paramElt.className += " visualizer"
    canvasElt.className += " visualizer"
  } else {
    bodyElt.className += " pattern"
    paramElt.className += " pattern"
  }

  document.body.appendChild(bodyElt)
  return bodyElt
}


function createParameterAdvocate(obj) {
  const paramElt = createElement(
    'div', { className: 'parameter advocate' })
  const paramSpan = createElement(
    'span', { className: 'parameter', innerHTML: obj.name }
  )

  paramElt.appendChild(paramSpan)
  for (const p in obj) {
    const elt = createPropertyElement(obj, p)
    if (elt !== undefined)
      paramElt.appendChild(elt)
  }

  return paramElt
}

function createPropertyElement(obj, property) {
  const attributes = getAttributes(obj, property)
  if (attributes === undefined)
    return

  const inputDiv = createElement(
    'div', { className: attributes.className + ' box', })

  const inputTitle = createElement(
    'div', { className: attributes.className + ' title', innerHTML: property })


  let inputElt = createElement(attributes.elementType, attributes)

  if (attributes.optionsAttributes !== undefined) {
    for (o in attributes.optionsAttributes) {
      const opt = createElement('option', attributes.optionsAttributes[o])
      inputElt.add(opt)
    }

  }

  inputElt.addEventListener("input",
    function () {
      const val = elt2ObjValue(inputElt, property)
      obj[property].value = val
      if (inputElt.type === 'range')
        inputTitle.innerHTML = property + ": " + obj[property].value.toFixed(2)

      if (obj[property].inputCallback !== undefined) {
        obj[property].inputCallback()
      }
    }
  )
  inputDiv.appendChild(inputTitle)
  inputDiv.appendChild(inputElt)
  const event = new CustomEvent("input");
  inputElt.dispatchEvent(event)
  return inputDiv
}

function getAttributes(obj, property) {
  if (obj[property].value === undefined)
    return
  const newAttributes = {
    name: property,
    className: 'input advocate',
    min: 0,
    max: obj[property].value,
    step: Number.MIN_VALUE,
  }

  switch (typeof obj[property].value) {
    case 'number':
      newAttributes.elementType = 'input'
      newAttributes.type = 'range'
      break;
    case 'boolean':
      newAttributes.elementType = 'input'
      newAttributes.type = 'checkbox'
      newAttributes.checked = obj[property].value
      break;
    default:
  }
  return Object.assign({},
    newAttributes,
    obj[property])
}


function elt2ObjValue(elt, property) {
  // console.log(elt, property)
  switch (elt.tagName) {
    case 'SELECT':
      // console.log(elt.optionsAttributes)
      return elt.optionsAttributes[elt.value].callback
      break;
    case 'INPUT':
      switch (elt.type) {
        case 'range':
          return parseFloat(elt.value)
        case 'checkbox':
          return elt.checked
        default:
          return elt.value
      }
      break;
    default:
      console.log('is an unsupported element type', elt)
      return elt.value
  }
}

function createElement(type, attributes) {
  return Object.assign(
    document.createElement(type),
    attributes)
}

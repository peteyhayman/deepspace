function createAnimator(properties) {

  const animator = {
    value: false,
    updateCallbacks: []
  }

  let counter = Date.now()
  let intervalId = undefined

  animator.getCounter = function () {
    return counter
  }

  animator.update = function () {
    counter = Date.now()
    animator.updateCallbacks.forEach(c => c())
  }

  animator.getDeltaTime = function () {
    return Date.now() - counter
  }

  animator.inputCallback = function () {
    if (animator.value && intervalId === undefined) {
      intervalId = setInterval(animator.update, 10)
    } else {
      clearInterval(intervalId)
      intervalId = undefined
    }
  }
  Object.assign(animator, properties)
  //animator.inputCallback()
  //console.log(animator.value)
  return animator
}
const allCoordinateSystems = {
  sets2Linear: {
    text: "Linear",
    value: 'sets2Linear',
    callback: sets2Linear
  },
  sets2Cartesian: {
    text: "Cartesian",
    value: 'sets2Cartesian',
    callback: sets2Cartesian
  },
  sets2Polar: {
    text: "Polar",
    value: 'sets2Polar',
    callback: sets2Polar
  }
}

function set2InterpolatedSet(set, resolution) {
  const deltaIndex = set.length / resolution
  const interpolatedSet = []
  for (let i = 0; i < resolution; i++) {
    const iFloat = i * deltaIndex;
    const i0 = Math.floor(iFloat)
    const i1 = (i0 + 1) % set.length //wraps around
    const t = iFloat % 1
    const val = lerp(set[i0], set[i1], t)
    interpolatedSet.push(val)
  }
  return interpolatedSet
}

function lerp(a, b, t) {
  return (b - a) * t + a
}

function sets2Cartesian(sets, scale) {
  if (sets === undefined)
    return
  return [sets[0].map((s, i) => {
    const x = sets[0][i];
    const y = sets.length > 1 ?
      sets[1][i] : sets[0][i]
    return {
      x: normalize(x * scale),
      y: normalize(y * scale)
    }
  })]
}

function sets2Linear(sets, scale) {
  return sets.map(set => {
    const deltaX = 1 / (set.length - 1)
    return set.map((v, i) => {
      return {
        x: i * deltaX,
        y: normalize(v * scale)
      }
    })
  })
}

function sets2Polar(sets, scale) {
  return sets.map(set => {
    const deltaTheta = (Math.PI * 2) / set.length
    const positionSet = set.map((v, i) => {
      const theta = i * deltaTheta
      const radius = normalize(v)
      return {
        x: normalize(Math.cos(theta) * radius * scale),
        y: normalize(Math.sin(theta) * radius * scale)
      }
    })
    //close the loop
    positionSet.push(positionSet[0])
    return positionSet
  })
}

function normalize(val) {
  return (val + 1) / 2
}


let travelRightFunction
let travelRightGenerator
let travelRightLinear
let travelRightRadial

let travelLeftFunction
let travelLeftGenerator
let travelLeftLinear
let travelLeftRadial

let standingGenerator
let standingLinear
let standingRadial

let roseFunction
let roseParametric

let drawers = []

function init() {
    const w = width / 3;
    const h = height / 3
    const f = 3;

    travelRightFunction = createFunction({ phase: t, frequency: f })

    roseFunction = createLissajoursPattern();
    //console.log(roseFunction)
    roseParametric = createDrawer({ callback: drawParametricPolar, functions: [roseFunction], x: 0, y: 0, width: width, height: height })


    travelRightLinear = createDrawer({ callback: drawLinear, functions: [travelRightFunction], x: 0, y: 0, width: w, height: h })
    travelRightRadial = createDrawer({ callback: drawRadial, functions: [travelRightFunction], x: w * 2, y: 0, width: w, height: h })
    //left renderer
    travelLeftFunction = createFunction({ phase: -t, frequency: f })
    travelLeftLinear = createDrawer({ callback: drawLinear, functions: [travelLeftFunction, travelRightFunction], x: 0, y: h, width: w, height: h })
    travelLeftParametric = createDrawer({ callback: drawParametric, functions: [travelLeftFunction, travelRightFunction], x: w * 2, y: h, width: w, height: h })
    //standing renderer

    standingFunction = createAdditiveFunction({ functions: [travelRightFunction, travelLeftFunction] })
    standingLinear = createDrawer({ callback: drawLinear, functions: [standingFunction], x: 0, y: h * 2, width: w, height: h })
    standingRadial = createDrawer({ callback: drawRadial, functions: [standingFunction], x: w * 2, y: h * 2, width: w, height: h })


    //console.log(roseParametric)
    drawers = [travelRightLinear, travelRightRadial, travelLeftLinear, travelLeftParametric, standingLinear, standingRadial, roseParametric]
    //console.log(drawers)
}
function setup() {
    createCanvas(window.innerWidth, window.innerHeight)
    background(0)
    init()


}

let t = 0
function draw() {
    background(0)
    travelRightFunction.phase = t;
    travelLeftFunction.phase = -t
    roseFunction.functions.forEach(f => f.phase = 2 * t)
    drawers.forEach(d => {
        // d.draw()
    })
    roseParametric.draw()
    t += 0.1
    //noLoop()
}

let bloop = false
function keyPressed() {
    bloop = !bloop
    if (bloop)
        loop()
    if (!bloop)
        noLoop()
}